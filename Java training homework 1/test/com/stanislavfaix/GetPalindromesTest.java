package com.stanislavfaix;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class GetPalindromesTest {

	private Palindrome pal;	
	
	@Before
    public void initialize() {
        pal = new PalindromeImpl();
    }
	
	@Test
	public void checkIfNullCollectionReturnsEmptyCollection()
	{
		Collection<String> out = pal.getPalindromes(null);
		assertEquals(0, out.size());
	}
	
	@Test
	public void checkIfFunctionDoesNotAffectItemsInCollection()
	{
		Collection<String> fiveStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, "abčcba", " ",
        		"Kobýla, má**malYb((o)*K)!!", "a bč c ba",
        		"Something really wrong!", "aBcCba" };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	fiveStringsPalindromes.add(originalCollection[i]);
        }
        
        pal.getPalindromes(fiveStringsPalindromes);
        String[] stringsAfter = new String[fiveStringsPalindromes.size()];
        fiveStringsPalindromes.toArray(stringsAfter);
        
        assertArrayEquals(originalCollection, stringsAfter);
	}
	
	@Test
	public void checkIfOutputCollectionIsDiffrentThanInputCollection()
	{
		Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, " ", "Something really wrong!" };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        Collection<String> out = pal.getPalindromes(oneStringsPalindromes);
        assertNotSame(oneStringsPalindromes, out);
	}

	@Test
	public void checkIfOutputCollectionIsNotNull()
	{
		Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, " ", "Something really wrong!" };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        Collection<String> out = pal.getPalindromes(oneStringsPalindromes);
        assertNotNull(out);
	}
	
	@Test
	public void checkIfCollectionAfterFunctionIsNotNull()
	{
		Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, " ", "Something really wrong!" };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        pal.getPalindromes(oneStringsPalindromes);
        assertNotNull(oneStringsPalindromes);
	}
}
