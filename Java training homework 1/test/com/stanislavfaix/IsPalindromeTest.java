package com.stanislavfaix;

import static org.junit.Assert.*;
import org.junit.*;

public class IsPalindromeTest {

	private Palindrome pal;	
	
	@Before
    public void initialize() {
        pal = new PalindromeImpl();
    }
	
	@Test
	public void stringWithSpacesIsPalindrome() {
		assertTrue(pal.isPalindrome("ab cba"));
	}
	
	@Test
	public void stringWithDiacriticsIsPalindrome() {
		assertTrue(pal.isPalindrome("abčcba"));
	}

	@Test
	public void stringWithDiacriticsAndSpacesIsPalindrome() {
		assertTrue(pal.isPalindrome("a bč c ba"));
	}
	
	@Test
	public void caseInsensitiveStringIsPalindrome() {
		assertTrue(pal.isPalindrome("aBcCba"));
	}
	
	@Test
	public void stringWithSpecialCharactersIsPalindrome() {
		assertTrue(pal.isPalindrome("kobyla, ma, maly, bok"));
	}
	
	@Test
	public void stringWithAllPossibleExceptionsCharactersIsPalindrome() {
		assertTrue(pal.isPalindrome("Kobýla, má**malYb((o)*K)!!"));
	}
	
	@Test
	public void nullIsNotPalindrome() {
		assertFalse(pal.isPalindrome(null));
	}
	
	@Test
	public void emptyStringIsNotPalindrome() {
		assertFalse(pal.isPalindrome(""));
	}
	
	@Test
	public void stringWithLessThanTwoCharactersIsNotPalindrome() {
		assertFalse(pal.isPalindrome("a"));
	}
	
	@Test
	public void stringWithLessThanTwoPlusSpacesCharactersIsNotPalindrome() {
		assertFalse(pal.isPalindrome("a        "));
	}
	
	@Test
	public void stringWithDiacriticsIsNotPalindrome() {
		assertFalse(pal.isPalindrome("tohle není palindom"));
	}
	
	@Test
	public void caseInsensitiveStringIsNotPalindrome() {
		assertFalse(pal.isPalindrome("TohlE neNI pAlindROM"));
	}
	
	@Test
	public void stringWithAllPossibleExceptionsCharactersIsNotPalindrome() {
		assertFalse(pal.isPalindrome("Mohyla, má**malYb((o)*K)!!"));
	}	
}