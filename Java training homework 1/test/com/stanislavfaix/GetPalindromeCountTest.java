package com.stanislavfaix;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class GetPalindromeCountTest  {

	private Palindrome pal;	
	
	@Before
    public void initialize() {
        pal = new PalindromeImpl();
    }
	
	@Test
	public void checkIfNullCollectionIsEqualToZero()
	{
		int actual = pal.getPalindromeCount(null);
        assertEquals(0, actual);
	}
	
	@Test
	public void checkIfEmptyCollectionIsEqualToZero()
	{
		Collection<String> zeroStringsPalindromes = new ArrayList<String>();
		int actual = pal.getPalindromeCount(zeroStringsPalindromes);
        assertEquals(0, actual);
	}
	
	@Test
	public void checkIfFivePalindromeCollectionIsEqualToFive()
	{
        Collection<String> fiveStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, "abčcba", " ",
        		"Kobýla, má**malYb((o)*K)!!", "a bč c ba",
        		"Something really wrong!", "aBcCba" };

        for(int i = 0; i < originalCollection.length; i++) {
        	fiveStringsPalindromes.add(originalCollection[i]);
        }
        
		int actual = pal.getPalindromeCount(fiveStringsPalindromes);
        assertEquals(5, actual);
	}
	
	@Test
	public void checkIfFunctionDoesNotAffectItemsInCollection()
	{
        Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, " ", "Something really wrong!" };

        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        pal.getPalindromeCount(oneStringsPalindromes);
        String[] expectedCollection = new String[oneStringsPalindromes.size()];
        oneStringsPalindromes.toArray(expectedCollection);
        
        assertArrayEquals(originalCollection, expectedCollection);
	}
	
	@Test
	public void checkIfFilledCollectionAfterFunctionIsNotNull()
	{
        Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "ab cba", null, " ", "Something really wrong!" };

        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        pal.getPalindromes(oneStringsPalindromes);
        assertNotNull(oneStringsPalindromes);
	}
	
	@Test
	public void checkIfEmptyCollectionAfterFunctionIsNotNull()
	{
        Collection<String> zeroStringsPalindromes = new ArrayList<String>();
        
        pal.getPalindromes(zeroStringsPalindromes);
        assertNotNull(zeroStringsPalindromes);
	}
}
