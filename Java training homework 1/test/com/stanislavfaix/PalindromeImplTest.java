package com.stanislavfaix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PalindromeImplTest {

	private PalindromeImpl pal;	
	
	@Before
    public void initialize() {
        pal = new PalindromeImpl();
    }
	
	@Test
	public void checkIfAfterConversionIsNotNull()
	{
		String s = "Something..";
        
		pal.convertToLowerAsciiOnly(s);
        assertNotNull(s);
	}
	
	@Test
	public void checkIfConversionReturnsNewObject()
	{
		String actual = "Something..";
        
		String expected = pal.convertToLowerAsciiOnly(actual);
		assertNotSame(actual, expected);
	}
	
	@Test
	public void checkIfConversionDoesntAffectParameter()
	{
		String s = "Something..";
		
		pal.convertToLowerAsciiOnly(s);
		assertEquals("Something..", s);
	}
	
	@Test
	public void nullParameterIsEqualToEmptyString() {
		String out = pal.convertToLowerAsciiOnly(null);
		assertEquals("", out);
	}
	
	@Test
	public void removeSpaces() {
		String out = pal.convertToLowerAsciiOnly("hi brad");
		assertEquals("hibrad", out);
	}
	
	@Test
	public void setLowerCase() {
		String out = pal.convertToLowerAsciiOnly("HiBrad");
		assertEquals("hibrad", out);
	}

	@Test
	public void removeSpecialCharacters() {
		String out = pal.convertToLowerAsciiOnly("h*i+b-r=a?d!");
		assertEquals("hibrad", out);
	}
	
	@Test
	public void removeDiacritics() {
		String out = pal.convertToLowerAsciiOnly("hibrád");
		assertEquals("hibrad", out);
	}
	
	@Test
	public void editAllDisallowedCharacters() {
		String out = pal.convertToLowerAsciiOnly("H!i brád.");
		assertEquals("hibrad", out);
	}
	
	@Test
	public void allowedParametrReturnsHimself() {
		String out = pal.convertToLowerAsciiOnly("hibrad");
		assertEquals("hibrad", out);
	}
}
