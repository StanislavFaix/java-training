package com.stanislavfaix;

import java.util.Collection;

public interface Palindrome {
	
	/**
	 * 
	 * @param source string variable to check
	 * @return <code>true</code> if <code>source</code> is palindrome otherwise returns <code>false</code>
	 */
	public boolean isPalindrome(String source);
	
	/**
	 * 
	 * @param source collection of strings to work with
	 * @return count of palindromes from source
	 */
	public int getPalindromeCount(Collection<String> source);
	
	/**
	 * 
	 * @param source collection of strings to work with
	 * @return string collection of palindromes from source
	 */
	public Collection<String> getPalindromes(Collection<String> source);
}
