package com.stanislavfaix;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class PalindromeGui {
		
	public static void main(String[] args) {
		
		JFrame frame = new JFrame();		
		frame.setTitle("Palindrome tester");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setPreferredSize(new Dimension(420, 64));
        
        Palindrome pal = new PalindromeImpl();
        
        JLabel label= new JLabel("Text:");
        JTextField inputField = new JTextField("", 30);
        
        JLabel imagelabel = new JLabel();
        imagelabel.setPreferredSize(new Dimension(18, 18));
        imagelabel.setBackground(Color.RED);
        imagelabel.setOpaque(true);
        imagelabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
                
        inputField.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			
			private void update()
			{
				if(pal.isPalindrome(inputField.getText())) {
					imagelabel.setBackground(Color.GREEN);
				} else {
					imagelabel.setBackground(Color.RED);
				}				
			}
		});

        JPanel panel = new JPanel();
        panel.add(label);
        panel.add(inputField);
        panel.add(imagelabel);
        frame.setContentPane(panel);        
        frame.pack();
        frame.setVisible(true);
	}

}
