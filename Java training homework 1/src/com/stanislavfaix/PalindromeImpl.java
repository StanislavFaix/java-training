package com.stanislavfaix;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;

public class PalindromeImpl implements Palindrome {

	@Override
	public boolean isPalindrome(String source) {
		if(source == null) {
			return false;
		}		
		source = convertToLowerAsciiOnly(source);
		
		if(source.length() < 2) {
			return false;
		}

		for(int i = 0; i < source.length() / 2; i++) {
			if(source.charAt(i) != source.charAt(source.length() - (1 + i))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param source string to work with
	 * @return string without spaces, accents, spaces and in lower characters only
	 */
	String convertToLowerAsciiOnly(String source)
	{
		if(source == null)
		{
			return new String();
		}
		
		String out = Normalizer.normalize(source, Normalizer.Form.NFD);
		out = out.replaceAll("[^\\p{ASCII}[^\\p{L}\\p{Z}[ ]]]", "").toLowerCase();
		return out;
	}

	@Override
	public int getPalindromeCount(Collection<String> source) {	
		if(source == null)
		{
			return 0;
		}
		
		int count = 0;		
		for (String s : source) {
			if (isPalindrome(s)) {
				count++;
			}
		}
		
		return count;
	}

	@Override
	public Collection<String> getPalindromes(Collection<String> source) {		
		Collection<String> out = new ArrayList<String>();		
		if(source == null)
		{
			return out;
		}
		
		for (String s : source) {
			if (isPalindrome(s)) {
				out.add(s);
			}
		}		
		return out;
	}

}
