package com.javatrainingteam.pin;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class PinGui {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("SSN/PIN tester");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setPreferredSize(new Dimension(500, 230));                
        
        JPanel verticalPanel = new JPanel();
        verticalPanel.setLayout(new BoxLayout(verticalPanel, BoxLayout.Y_AXIS));
        verticalPanel.setBorder(new EmptyBorder(10, 20, 20, 20));
        verticalPanel.add(createPanel("Finland:", new FinnishPin()));
        verticalPanel.add(createPanel("Sweden:", new SwedishPin()));
        verticalPanel.add(createPanel("Norway:", new NorwegianPin()));
        verticalPanel.add(createPanel("Denmark:", new DanishPin()));
        verticalPanel.add(createPanel("Czech Rep.:", new CzechPin()));        
        
        frame.setContentPane(verticalPanel);        
        frame.pack();
        frame.setVisible(true);
	}

	private static JPanel createPanel(String country, PersonalIdentificationNumber pin) {
		JLabel label = new JLabel(country);
		label.setPreferredSize(new Dimension(70, 36));
		
		JLabel imageLabel = new JLabel();
        imageLabel.setPreferredSize(new Dimension(18, 18));
        imageLabel.setBackground(Color.RED);
        imageLabel.setOpaque(true);
        imageLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        
        JTextField inputField = new JTextField("", 30);
        inputField.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			
			private void update()
			{
				if(pin != null && pin.isPin(inputField.getText())) {
					imageLabel.setBackground(Color.GREEN);
				} else {
					imageLabel.setBackground(Color.RED);
				}				
			}
		});
		
		JPanel panel = new JPanel();
		panel.add(label);
		panel.add(inputField);
		panel.add(imageLabel);
		
		return panel;
	}
}
