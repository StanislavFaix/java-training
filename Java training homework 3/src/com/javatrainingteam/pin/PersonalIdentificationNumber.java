/**
 * 
 */
package com.javatrainingteam.pin;

public interface PersonalIdentificationNumber {
	
	/**
	 * 
	 * @param source string variable to check
	 * @return <code>true</code> if <code>source</code> is pin otherwise returns <code>false</code>
	 */
	public boolean isPin(String source);
}