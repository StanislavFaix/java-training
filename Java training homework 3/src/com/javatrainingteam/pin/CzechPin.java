package com.javatrainingteam.pin;

public class CzechPin implements PersonalIdentificationNumber {

	@Override
	public boolean isPin(String source) {
		if(source == null || !(source.matches("(\\d{6})/(\\d{4})") || source.matches("(\\d{6})/(\\d{3})"))) {
			return false;
		}
		source = source.replace("/", "");
		
		if(!isDateValid(source)) {
			return false;
		}
		
		//before 1954 had only 9 digits
		if(source.length() == 9 && Integer.parseInt(source.substring(0, 2)) < 54) {
			return true;
		}
		
		//in 1955 was implemented 10th digit
		if(source.length() == 9) {
			return false;
		}

		/*I am little confused about algorithm here, wikipedia.org says two different things. Last
		 * digit is modulo of first 9 digits and 11. When modulo is 10, last digit should be 0,
		 * but first 9 digits 570707004 is not allowed, what wiki?
		 * 
		 * Another page says first 9 digits 570707004 is allowed, modulo is 10 so last digit is 0,
		 * but their algorithm is not perfect(registry office management).
		 * source: https://phpfashion.com/jak-overit-platne-ic-a-rodne-cislo
		 */
		int firstDigits = Integer.parseInt(source.substring(0, 9));
		int lastDigit = Integer.parseInt(source.substring(9, 10));
		
		int modulo = firstDigits % 11;
		if(modulo == lastDigit || (modulo == 10 && lastDigit == 0)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param source string in which it is investigated whether the first six digits is the date
	 * @return <code>true</code> if <code>source</code> is correct date, otherwise returns <code>false</code>
	 */
	boolean isDateValid(String source) {
		int year = Integer.parseInt(source.substring(0, 2));
		int month = Integer.parseInt(source.substring(2, 4));
		int day = Integer.parseInt(source.substring(4, 6));
		
		//check if date is valid and day is not 0, otherwise returns false
		if((!isMan(source) && !isWoman(source)) || day == 0) {
			return false;
		}

		//optimize month if is woman
		if(isWoman(source)) {
			month -= 50;			
		}

		//sometimes month is +20
		if(month > 20) {
			month -= 20;
		}
		
		//optimize day if is foreigner
		if(isForeigner(source)) {
			day -= 50;
		}
		
		if(month > 12) {
			return false;
		}

		//check if day is in month range
		if(month == 2) {
			if(year % 4 == 0 && day <= 29) {
				return true;
			}
			else if(day <= 28) {
				return true;
			}
		}
		else if(month == 4 || month == 6 || month == 9 || month == 11) {
			if(day <= 30) {
				return true;
			}
		} else if(day <= 31) {
			return true;
		}			
		return false;
	}
	
	/**
	 * 
	 * @param source string in which it is investigated whether is woman
	 * @return <code>true</code> if <code>source</code> is woman pin, otherwise returns <code>false</code>
	 */
	boolean isWoman(String source) {
		int month = Integer.parseInt(source.substring(2, 4));
		
		//women have +50 to month
		if((month > 50 && month <= 62) || (month > 70 && month <= 82)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param source string in which it is investigated whether is man
	 * @return <code>true</code> if <code>source</code> is man pin, otherwise returns <code>false</code>
	 */
	boolean isMan(String source) {
		int month = Integer.parseInt(source.substring(2, 4));
		
		//men have month in 1..12
		if((month > 0 && month <= 12) || (month > 20 && month <= 32)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param source string in which it is investigated whether is foreigner
	 * @return <code>true</code> if <code>source</code> is foreigner pin, otherwise returns <code>false</code>
	 */
	boolean isForeigner(String source) {
		int day = Integer.parseInt(source.substring(4, 6));
		
		//foreigners have +50 to day
		if(day > 50 && day <= 81) {
			return true;
		}
		return false;
	}
}
