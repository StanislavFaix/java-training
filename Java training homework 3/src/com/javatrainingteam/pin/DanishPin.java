package com.javatrainingteam.pin;

public class DanishPin implements PersonalIdentificationNumber {

	@Override
	public boolean isPin(String source) {
		if(source == null || !source.matches("(\\d{6})-(\\d{4})")) {
			return false;
		}
		source = source.replace("-", "");
		if(!isDateValid(source)) {
			return false;
		}
		
		/*CPR number is split into 10 digits, and then multiplied by a factor.
		 * The factors are predefined and are: 4, 3, 2, 7, 6, 5, 4, 3, 2, 1.
		 * Once each digit is multiplied, the sum of all digits are added together,
		 * and pulled through modulus with 11. If the result is (11 - last number) it is a valid CPR number.
		 */
		int[] controlTable = new int[] { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
		
		int sum = 0;
		for (int i = 0; i < source.length() - 1; i++) {
			sum += (source.charAt(i) - 48) * controlTable[i];
		}
		
		if (sum % 11 == 11 - (source.charAt(9) - 48)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param source string in which it is investigated whether the first six digits is the date
	 * @return <code>true</code> if <code>source</code> is correct date, otherwise returns <code>false</code>
	 */
	boolean isDateValid(String source) {
		int day = Integer.parseInt(source.substring(0, 2));
		int month = Integer.parseInt(source.substring(2, 4));
		
		//check if date is valid and day is not 0, otherwise returns false
		if(month > 12 || month == 0 || day == 0) {
			return false;
		}

		//check if day is in month range
		if(month == 2) {
			int year = Integer.parseInt(source.substring(4, 6));
			if(year % 4 == 0 && day <= 29) {
				return true;
			}
			else if(day <= 28) {
				return true;
			}
		}
		else if(month == 4 || month == 6 || month == 9 || month == 11) {
			if(day <= 30) {
				return true;
			}
		} else {
			if(day <= 31) {
				return true;
			}
		}			
		return false;
	}
}

