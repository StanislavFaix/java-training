package com.javatrainingteam.pin;



public class NorwegianPin implements PersonalIdentificationNumber {

	@Override
	public boolean isPin(String pin) {
		if (pin==null|pin=="") {
			return false;
		}
		
		String pinClear=getPINWithoutSpecialCharacters(pin);
		
		if ((pinClear.length()!=11)||(!isNumeric(pinClear))){
			return false;
		}
		
		if (getChecksumForValidation(pinClear).equals(pinClear.substring(9))) {
			return true;
		}
		return false;
	}

	
	/**
	 * @param only 11-digits pin string
	 * @return recalculated checksum norwegian pin
	 */
	private String getChecksumForValidation(String pin) {
		int[]numberOfPin=new int [pin.length()];
		for (int i=0;i<pin.length();i++){
			int suppPin=Integer.parseInt(pin.substring(i,i+1));
			numberOfPin[i]=suppPin;
		}
		
		int cc1=0;
		cc1 = 11 - (
	                		 (3 * numberOfPin[0]
	                        + 7 * numberOfPin[1]
	                        + 6 * numberOfPin[2]
	                        + 1 * numberOfPin[3]
	                        + 8 * numberOfPin[4]
	                        + 9 * numberOfPin[5]
	                        + 4 * numberOfPin[6]
	                        + 5 * numberOfPin[7]
	                        + 2 * numberOfPin[8]) % 11);
			if (cc1==11) {
				cc1=0;
			}
			
			int cc2=0;
			cc2 = 11 - (
           		 			 (5 * numberOfPin[0]
           				 	+ 4 * numberOfPin[1]
                   			+ 3 * numberOfPin[2]
                   			+ 2 * numberOfPin[3]
                   			+ 7 * numberOfPin[4]
                   			+ 6 * numberOfPin[5]
                   			+ 5 * numberOfPin[6]
                   			+ 4 * numberOfPin[7]
                   			+ 3 * numberOfPin[8]
							+ 2 * cc1) % 11);   
		if (cc2==11) {
            cc2 = 0;     
        }
		return (Integer.toString(cc1))+(Integer.toString(cc2));
	}
	
	/**
	 * @param pin with any other special characters
	 * @return converted string without special characters
	 */
	String getPINWithoutSpecialCharacters(String pin) {
		pin=pin.replace(" ", "");
		pin=pin.replace("/", "");
		pin=pin.replace("-", "");
		return pin;
	}
	
	/**
	 * @param input string without slash, spaces and dash
	 * @return <code>true</code> if input string contains only numbers,otherwise <code>false</code> 
	 */
	boolean isNumeric(String input){
		return input.matches("((-|\\+)?[0-9]+(\\[0-9]+)?)+");
	}
	
}
