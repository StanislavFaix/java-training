package com.javatrainingteam.pin;

public class SwedishPin implements PersonalIdentificationNumber{
	public boolean isPin (String source) {
		int YYMMDD;
		int temporary = 0;
		int sumSingleNummer = 0;
		
		if (source == null || source.length() != 11) {
			return false;
		}
		
		if ((isMonthInPersonnummer(source) && isDayInPersonnummer(source))) {			
						
			for (int i = 0; i < 9; i++) {
				YYMMDD = Integer.parseInt(source.replaceAll("[^0-9]", "").substring(i,i+1));
				
				temporary = YYMMDD;
				
				if ((i % 2) == 0) {
					temporary = YYMMDD * 2;
					
					if (temporary > 10) {
						sumSingleNummer = sumSingleNummer + 1 + (temporary % 10);						
					}
					else {
						sumSingleNummer = sumSingleNummer + temporary;
					}					
				}
				else {					
					sumSingleNummer = sumSingleNummer +  YYMMDD;					
				}				
			}	
		}	
		
		if (isZero(sumSingleNummer) != Integer.parseInt(source.substring(10, 11))) {	
			return false;
		}
		else {
			return true;
		}	
	}	

	private int isZero(int sumSingleNummer) {
		if ((10 - (sumSingleNummer % 10)) == 10) {
			return 0;
		}
		else {
			return (10 - (sumSingleNummer % 10));
		}
	}

//	private boolean isHyphenInPersonnummer(String source) {
//		char hyphen = source.charAt(6);
//		if( hyphen =='-' )
//		{
//			return true;
//		}
//		else {
//			return false;
//		}	
//	}

	
	private boolean isDayInPersonnummer(String source) {
		int day = Integer.parseInt(source.substring(4,6));
		if( day > 0 || day < 31 ) 
		{
			return true;
		}
		else {
			return false;
		}	
	}	
	
	private boolean isMonthInPersonnummer(String source) {
		int month = Integer.parseInt(source.substring(2,4));
		if( month > 0 || month < 12) 
		{
			return true;
		}
		else {
			return false;
		}
	}
}
