package com.javatrainingteam.pin;
import java.util.regex.*;

public class FinnishPin implements PersonalIdentificationNumber {
	@Override
	public boolean isPin(String source) {
		if (source == null){
			return false;
		}
		else{
			if( regexValidationFinnishId(source))
			{	
				if (lastDigitEgualsToReminderFromDivision(source))
					return true;
				else 
					return false;
			}
			else {
				return false;
			}
		}
	}
	/**
	 * compares formal structure of Finnish ID(DDMMYYCZZZQ) with given source
	 * @param source string
	 * @return true if source string corresponds to ID structure 
	 */
	boolean regexValidationFinnishId(String source){
		String regex = "^(((0[1-9]|1[0-9]|2[0-8])(0[1-9]|1[012]))|((29|30|31)(0[13578]|1[02]))|((29|30)(0[469]|11)))([0-9][0-9])([A+-])(([0-8][0-9][2-9])|([0-8][1-9][0-9])|([1-8][0-9][0-9]))([0-9A-Z])$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(source);
		if(!matcher.matches())
			return false;
		else 
			return true;
	}
	/**
	 * compares last character of the string with correspondent valid characters (letters stands for digits e.g.A=10)
	 * @param source string
	 * @return true if character correspond to the valid character
	 */
	boolean lastDigitEgualsToReminderFromDivision(String source){
		char lastCharacter = source.charAt(source.length()-1);
		char[] validControlEndCharacters = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','H','J','K','L','M','N','P','R','S','T','U','V','W','X','Y'};
		source = source.substring(0,source.length()-1);
		source = source.replaceAll("[A+-]","");
		int reminder = 0;
		int sourceNumber = Integer.parseInt(source);
		reminder = sourceNumber%31;
		if (lastCharacter==validControlEndCharacters[reminder])
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
