package com.stanislavfaix;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class GetDanishPinCountTest {

	private PersonalIdentificationNumber mDenmarkPin;
	
	@Before
    public void initialize() {
		mDenmarkPin = new DenmarkPin();		
    }
	
	@Test
	public void checkIfNullCollectionIsEqualToZero()
	{
		int actual = mDenmarkPin.getPinCount(null);
        assertEquals(0, actual);
	}
	
	@Test
	public void checkIfEmptyCollectionIsEqualToZero()
	{
		Collection<String> emptyCollection = new ArrayList<String>();
		int actual = mDenmarkPin.getPinCount(emptyCollection);
        assertEquals(0, actual);
	}
	
	@Test
	public void checkIfFivePalindromeCollectionIsEqualToFive()
	{
        Collection<String> fiveStringsPins = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0535", "280885-0543", "280885-0551",
        		"280885-0578", "280885-0586", "abcdef-gh", "2808850004", null, "" };

        for(int i = 0; i < originalCollection.length; i++) {
        	fiveStringsPins.add(originalCollection[i]);
        }
        
		int actual = mDenmarkPin.getPinCount(fiveStringsPins);
        assertEquals(5, actual);
	}
	
	@Test
	public void checkIfAfterCountingDoesNotAffectItemsInCollection()
	{
        Collection<String> someDanishPins = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0004",	"280885-0012", "280885-0020", null };

        for(int i = 0; i < originalCollection.length; i++) {
        	someDanishPins.add(originalCollection[i]);
        }
        
        mDenmarkPin.getPinCount(someDanishPins);
        String[] expectedCollection = new String[someDanishPins.size()];
        someDanishPins.toArray(expectedCollection);
        
        assertArrayEquals(originalCollection, expectedCollection);
	}
	
	@Test
	public void checkIfFilledCollectionAfterFunctionIsNotNull()
	{
        Collection<String> someDanishPins = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0004",	"280885-0012", "280885-0020", null };

        for(int i = 0; i < originalCollection.length; i++) {
        	someDanishPins.add(originalCollection[i]);
        }
        
        mDenmarkPin.getPinCount(someDanishPins);
        assertNotNull(someDanishPins);
	}
	
	@Test
	public void checkIfEmptyCollectionAfterCountingIsNotNull()
	{
        Collection<String> emptyCollection = new ArrayList<String>();
        
        mDenmarkPin.getPinCount(emptyCollection);
        assertNotNull(emptyCollection);
	}

}
