package com.stanislavfaix;

import static org.junit.Assert.*;
import org.junit.Test;

public class IsDateValidTest {
	
	@Test
	public void testLotOfInvalidDanishDates() {
		DenmarkPin denmarkPin = new DenmarkPin();	
		assertFalse(denmarkPin.isDateValid("3201010000"));		
		assertFalse(denmarkPin.isDateValid("2902010000"));
		assertFalse(denmarkPin.isDateValid("3203010000"));
		assertFalse(denmarkPin.isDateValid("3104010000"));
		assertFalse(denmarkPin.isDateValid("3205010000"));
		assertFalse(denmarkPin.isDateValid("3106010000"));
		assertFalse(denmarkPin.isDateValid("3207010000"));		
		assertFalse(denmarkPin.isDateValid("3208010000"));
		assertFalse(denmarkPin.isDateValid("3109850000"));
		assertFalse(denmarkPin.isDateValid("3210010000"));
		assertFalse(denmarkPin.isDateValid("3111010000"));
		assertFalse(denmarkPin.isDateValid("3212010000"));	
		assertFalse(denmarkPin.isDateValid("0000000000"));
		assertFalse(denmarkPin.isDateValid("0113010000"));	
	}

	@Test
	public void testLotOfValidDanishDates() {
		DenmarkPin denmarkPin = new DenmarkPin();	
		assertTrue(denmarkPin.isDateValid("3101010000"));
		assertTrue(denmarkPin.isDateValid("2902000000"));
		assertTrue(denmarkPin.isDateValid("3103010000"));
		assertTrue(denmarkPin.isDateValid("3004010000"));
		assertTrue(denmarkPin.isDateValid("3105010000"));
		assertTrue(denmarkPin.isDateValid("3006010000"));
		assertTrue(denmarkPin.isDateValid("3107010000"));
		assertTrue(denmarkPin.isDateValid("3108010000"));
		assertTrue(denmarkPin.isDateValid("3009850000"));
		assertTrue(denmarkPin.isDateValid("3110010000"));
		assertTrue(denmarkPin.isDateValid("3011010000"));
		assertTrue(denmarkPin.isDateValid("3112010000"));
	}
	
	@Test
	public void testLotOfInvalidCzechDates() {
		CzechPin czechPin = new CzechPin();		
		assertFalse(czechPin.isDateValid("0101320000"));
		assertFalse(czechPin.isDateValid("0102290000"));
		assertFalse(czechPin.isDateValid("0103320000"));
		assertFalse(czechPin.isDateValid("0104310000"));
		assertFalse(czechPin.isDateValid("0105320000"));
		assertFalse(czechPin.isDateValid("0106310000"));
		assertFalse(czechPin.isDateValid("0107320000"));
		assertFalse(czechPin.isDateValid("0108320000"));
		assertFalse(czechPin.isDateValid("0109310000"));
		assertFalse(czechPin.isDateValid("0110320000"));
		assertFalse(czechPin.isDateValid("0111310000"));
		assertFalse(czechPin.isDateValid("0112320000"));	
		assertFalse(czechPin.isDateValid("0000000000"));
		assertFalse(czechPin.isDateValid("0113010000"));
	}

	@Test
	public void testLotOfValidCzechDates() {
		CzechPin czechPin = new CzechPin();
		assertTrue(czechPin.isDateValid("0101310000"));
		assertTrue(czechPin.isDateValid("0002290000"));
		assertTrue(czechPin.isDateValid("0103310000"));
		assertTrue(czechPin.isDateValid("0104300000"));
		assertTrue(czechPin.isDateValid("0105310000"));
		assertTrue(czechPin.isDateValid("0106300000"));
		assertTrue(czechPin.isDateValid("0107310000"));
		assertTrue(czechPin.isDateValid("0108814000"));
		assertTrue(czechPin.isDateValid("0109804100"));
		assertTrue(czechPin.isDateValid("0110814200"));
		assertTrue(czechPin.isDateValid("0111804300"));
		assertTrue(czechPin.isDateValid("0112819900"));
		assertTrue(czechPin.isDateValid("0151300000"));
		assertTrue(czechPin.isDateValid("0162310000"));
		assertTrue(czechPin.isDateValid("0171300000"));
		assertTrue(czechPin.isDateValid("0182310000"));
	}
}
