package com.stanislavfaix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IsDanishPinTest {
	
	private PersonalIdentificationNumber mDenmarkPin;
	
	@Before
    public void initialize() {
		mDenmarkPin = new DenmarkPin();		
    }
	
	@Test
	public void afterCallingIsPinParameterIsNotChanged() {
		String pin = "280885-0004";
		mDenmarkPin.isPin(pin);
		assertEquals("280885-0004", pin);
	}

	@Test
	public void nullIsInvalidPin() {
		assertFalse(mDenmarkPin.isPin(null));
	}

	@Test
	public void emptyStringIsInvalidPin() {
		assertFalse(mDenmarkPin.isPin(""));
	}
	
	@Test
	public void denmarkPinIsValid() {
		assertTrue(mDenmarkPin.isPin("280885-0004"));
	}
	
	@Test
	public void denmarkPinWithoutDashIsInvalid() {
		assertFalse(mDenmarkPin.isPin("2808850004"));
	}
	
	@Test
	public void testLotOfInvalidPins() {
		assertFalse(mDenmarkPin.isPin("abcdef-gh"));
		assertFalse(mDenmarkPin.isPin("abcdefgh"));
		assertFalse(mDenmarkPin.isPin("abcdef-ghi"));
		assertFalse(mDenmarkPin.isPin("abcdef-g"));
		assertFalse(mDenmarkPin.isPin("a0b0c0-d0e0"));
		assertFalse(mDenmarkPin.isPin("280885-000"));
		assertFalse(mDenmarkPin.isPin("280885-00045"));
		assertFalse(mDenmarkPin.isPin("28088500045"));
	}
}
