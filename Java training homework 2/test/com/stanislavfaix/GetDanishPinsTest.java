package com.stanislavfaix;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class GetDanishPinsTest {

	private PersonalIdentificationNumber mDenmarkPin;
	
	@Before
    public void initialize() {
		mDenmarkPin = new DenmarkPin();		
    }

	@Test
	public void checkIfNullCollectionReturnsEmptyCollection()
	{
		Collection<String> out = mDenmarkPin.getPins(null);
		assertEquals(0, out.size());
	}
	
	@Test
	public void checkIfDoesNotAffectItemsInCollection()
	{
		Collection<String> fiveStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0004",	"280885-0012", "280885-0020", null };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	fiveStringsPalindromes.add(originalCollection[i]);
        }
        
        mDenmarkPin.getPins(fiveStringsPalindromes);
        String[] stringsAfter = new String[fiveStringsPalindromes.size()];
        fiveStringsPalindromes.toArray(stringsAfter);
        
        assertArrayEquals(originalCollection, stringsAfter);
	}
	
	@Test
	public void checkIfOutputCollectionIsDiffrentThanInputCollection()
	{
		Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0004",	"280885-0012", "280885-0020", null };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        Collection<String> out = mDenmarkPin.getPins(oneStringsPalindromes);
        assertNotSame(oneStringsPalindromes, out);
	}

	@Test
	public void checkIfOutputCollectionIsNotNull()
	{
		Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0004",	"280885-0012", "280885-0020", null };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        Collection<String> out = mDenmarkPin.getPins(oneStringsPalindromes);
        assertNotNull(out);
	}
	
	@Test
	public void checkIfCollectionAfterFunctionIsNotNull()
	{
		Collection<String> oneStringsPalindromes = new ArrayList<String>();
        String[] originalCollection = new String[] { "280885-0004",	"280885-0012", "280885-0020", null };
        
        for(int i = 0; i < originalCollection.length; i++) {
        	oneStringsPalindromes.add(originalCollection[i]);
        }
        
        mDenmarkPin.getPins(oneStringsPalindromes);
        assertNotNull(oneStringsPalindromes);
	}
}
