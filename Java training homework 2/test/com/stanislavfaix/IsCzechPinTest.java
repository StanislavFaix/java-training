package com.stanislavfaix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IsCzechPinTest {

	private PersonalIdentificationNumber mCzechPin;
	
	@Before
    public void initialize() {
		mCzechPin = new CzechPin();		
    }
	
	@Test
	public void afterCallingIsPinParameterIsNotChanged() {
		String pin = "850828/0004";
		mCzechPin.isPin(pin);
		assertEquals("850828/0004", pin);
	}

	@Test
	public void nullIsInvalidPin() {
		assertFalse(mCzechPin.isPin(null));
	}

	@Test
	public void emptyStringIsInvalidPin() {
		assertFalse(mCzechPin.isPin(""));
	}
	
	@Test
	public void denmarkPinWithoutSlashIsInvalid() {
		assertFalse(mCzechPin.isPin("8405011330"));
	}
	
	@Test
	public void testLotOfValidPins() {
		assertTrue(mCzechPin.isPin("010731/0005"));
		assertTrue(mCzechPin.isPin("010881/4013"));
		assertTrue(mCzechPin.isPin("010980/4123"));
		assertTrue(mCzechPin.isPin("011081/4231"));
		assertTrue(mCzechPin.isPin("011180/4341"));
		assertTrue(mCzechPin.isPin("011281/9950"));
		assertTrue(mCzechPin.isPin("015130/0061"));
		assertTrue(mCzechPin.isPin("016231/0071"));
		assertTrue(mCzechPin.isPin("017130/0085"));
		assertTrue(mCzechPin.isPin("018231/0095"));
	}
	
	@Test
	public void testLotOfInvalidPins() {
		assertFalse(mCzechPin.isPin("0107310005"));
		assertFalse(mCzechPin.isPin("010881//4013"));
		assertFalse(mCzechPin.isPin("011081231"));
		assertFalse(mCzechPin.isPin("851180/434"));	//good format(9 digits), but after 1954 has to be 10 digits
		assertFalse(mCzechPin.isPin("011281/9951"));
		assertFalse(mCzechPin.isPin("015130/0063"));
		assertFalse(mCzechPin.isPin("016231/0072"));
		assertFalse(mCzechPin.isPin("017130/0081"));
		assertFalse(mCzechPin.isPin("018231/0090"));
	}

}
