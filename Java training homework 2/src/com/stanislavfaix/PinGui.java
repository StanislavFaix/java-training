package com.stanislavfaix;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class PinGui {

	public static void main(String[] args) {
		JFrame frame = new JFrame();		
		frame.setTitle("SSN/PIN tester");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setPreferredSize(new Dimension(500, 230));
                
        JTextField[] inputField = new JTextField[5];
        JLabel[] imagelabel = new JLabel[5];
        JPanel[] panel = new JPanel[5];
        JLabel[] label = new JLabel[5];
        label[0]= new JLabel("Finland:");
        label[1]= new JLabel("Sweden:");
        label[2]= new JLabel("Norway:");
        label[3]= new JLabel("Denmark:");
        label[4]= new JLabel("Czech Rep.:"); 
        JPanel verticalPanel = new JPanel();
        verticalPanel.setLayout(new BoxLayout(verticalPanel, BoxLayout.Y_AXIS));
        verticalPanel.setBorder(new EmptyBorder(10, 20, 20, 20));
        
        for(int i = 0; i < 5; ++i) {
        	label[i].setPreferredSize(new Dimension(70, 36));
        	inputField[i] = new JTextField("", 30);
        	imagelabel[i] = new JLabel();
	        imagelabel[i].setPreferredSize(new Dimension(18, 18));
	        imagelabel[i].setBackground(Color.RED);
	        imagelabel[i].setOpaque(true);
	        imagelabel[i].setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
	        panel[i] = new JPanel();
	        panel[i].add(label[i]);
	        panel[i].add(inputField[i]);
	        panel[i].add(imagelabel[i]);
	        verticalPanel.add(panel[i]);
        }
        
        PersonalIdentificationNumber denmarkPin = new DenmarkPin();   
        inputField[3].getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			
			private void update()
			{
				if(denmarkPin.isPin(inputField[3].getText())) {
					imagelabel[3].setBackground(Color.GREEN);
				} else {
					imagelabel[3].setBackground(Color.RED);
				}				
			}
		});   
        
        PersonalIdentificationNumber czechPin = new CzechPin();   
        inputField[4].getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			
			private void update()
			{
				if(czechPin.isPin(inputField[4].getText())) {
					imagelabel[4].setBackground(Color.GREEN);
				} else {
					imagelabel[4].setBackground(Color.RED);
				}				
			}
		}); 
        
        frame.setContentPane(verticalPanel);        
        frame.pack();
        frame.setVisible(true);
	}

}
