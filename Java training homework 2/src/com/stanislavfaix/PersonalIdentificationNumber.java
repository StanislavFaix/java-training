package com.stanislavfaix;

import java.util.Collection;

public interface PersonalIdentificationNumber {
	
	/**
	 * 
	 * @param source string variable to check
	 * @return <code>true</code> if <code>source</code> is pin otherwise returns <code>false</code>
	 */
	public boolean isPin(String source);
	
	/**
	 * 
	 * @param source collection of strings to work with
	 * @return count of pins from source
	 */
	public int getPinCount(Collection<String> source);
	
	/**
	 * 
	 * @param source collection of strings to work with
	 * @return string collection of pins from source
	 */
	public Collection<String> getPins(Collection<String> source);
}
